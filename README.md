# old-mint-y

Just an archive of the old icons and themes from Linux Mint's Mint-Y (and Mint-X) style.
These can be directly copied into /usr/share/icons and /usr/share/themes.
